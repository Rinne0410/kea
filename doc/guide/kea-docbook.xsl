<?xml version='1.0'?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                 version="1.0">

  <xsl:import href="http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl"/>

  <!-- override encoding from ISO-8859-1 to UTF-8 -->
  <xsl:output method="html"
              encoding="UTF-8"
              indent="no"/>

</xsl:stylesheet>
